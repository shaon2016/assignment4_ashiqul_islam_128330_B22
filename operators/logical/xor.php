<?php

// $a xor $b	Xor	TRUE
// if either $a or $b is TRUE, but not both.

$a = 20;
$b = 21;
$c = 22;
$d = 22;

if(($a==$b) xor ($c==$d))
    echo "condtion is true". "<hr>";

$a = 21;
$b = 21;
$c = 22;
$d = 22;

if(($a==$b) xor ($c==$d)) // because both condition are true
    echo "condtion is true";
else echo "false";