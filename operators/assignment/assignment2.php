<?php

$a = 10;

$b = &$a;

echo $b."<hr>"; // assigning by reference

// If we change a b will aslo change . b variable will show
// the a's value

$a = 22;

echo $b; // b = 22