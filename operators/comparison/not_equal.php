<?php

// $a != $b	Not equal
//TRUE if $a is not equal to $b after type juggling.

$a = 5;
$b = $a;

echo $b."<hr>";

if ($a != $b)
    echo "a is not equal to b";
else
    echo "a is equal to b";