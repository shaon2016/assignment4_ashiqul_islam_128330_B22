<?php

//$a !== $b	Not identical
//TRUE if $a is not equal to $b, or they are not of the same type.

$a = $b = 10;


if ($a !== $b)  // false
    echo " a is not equal to b or not identical". "<hr>";
else
    echo "a is equal to b and identical". "<hr>";

$a = 3;
$b = 2.2;


if ($a !== $b) // true
    echo " a is not equal to b or not identical". "<hr>";
else
    echo "a is equal to b and identical". "<hr>";


$a = 3;
$b = 4;


if ($a !== $b)
    echo " a is not equal to b or not identical". "<hr>";
else
    echo "a is equal to b and identical". "<hr>";