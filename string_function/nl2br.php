<?php

//nl2br — Inserts HTML line breaks before all newlines in a string

// It will not show the actual newline output
echo "Md \nAshiqul \nIslam"."<hr>";

echo nl2br("Md \nAshiqul \nIslam");