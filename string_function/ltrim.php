<?php

//ltrim — Strip whitespace (or other characters)
// from the beginning of a string
echo ltrim(" ashiqul"). "<hr>";

echo ltrim("ashiqul", "a"). "<hr>";

echo ltrim("ashiqul", "ash"). "<hr>";

echo ltrim("ashiqul", "aski");