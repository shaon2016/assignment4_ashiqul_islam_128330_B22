<?php

//ucwords — Uppercase the first character of each word in a string

echo ucwords("international islamic
        university chittagong");

// Output

// International Islamic University Chittagong