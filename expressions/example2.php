<?php

// This part will complete the function expression part

function add($a, $b)
{
    return $a + $b;
}

echo add(2, 3);
echo "<hr>";

$p = 3;
$q = 4;

echo add(++$q, ++$q); // 5 + 6
echo "<hr>";

echo add($q, ++$q); // 6 + 7
echo "<hr>";

echo add($p++, ++$p); //3 + 5

